﻿using System;
using System.Collections.Generic;
using BleScan.ViewModels;
using BluetoothLE.Core;

using Xamarin.Forms;

namespace BleScan.Views
{
	public partial class ServicesPage : ContentPage
	{
		private ServicesPageViewModel ViewModel;
		public ServicesPage()
		{
			InitializeComponent();
			ViewModel = (ServicesPageViewModel)BindingContext;
			NavigationPage.SetHasNavigationBar(this, true);
		}

		public async void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (e.Item == null)
				return; 

			await ((ServicesPageViewModel)this.BindingContext).OnItemSelectedCommand.Execute((IService)e.Item);

			((ListView)sender).SelectedItem = null;
		}

	}
}
