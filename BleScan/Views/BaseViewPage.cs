﻿using BleScan.ViewModels;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace BleScan
{
	public class BaseViewPage : ContentPage
	{
		public BaseViewPage()
		{
		}

		/// <summary>
		/// Ons press of the physical back button.
		/// </summary>
		protected override bool OnBackButtonPressed()
		{
			// If you want to continue going back
			base.OnBackButtonPressed();
			return false;

			// If you want to stop the back button
			return true;
		}

		private void ChangeNavBar()
		{
			// hide the navigation bar, since we will be creating our own
			NavigationPage.SetHasNavigationBar(this, false);

			// build our custom navigation bar*******************************************
			var myNav = new ContentView();
			myNav.BackgroundColor = Color.Red;
			//myNav.WidthRequest = ParentView.Width;
			myNav.WidthRequest = 1024;

			var btnBack = new Image();
			btnBack.Source = "BackButton.png";
			btnBack.GestureRecognizers.Add(new TapGestureRecognizer((view) =>
			{
				Navigation.PopAsync();
			}));

			var title = new Label { Text = Title, HorizontalOptions = LayoutOptions.CenterAndExpand };

			var toprightlogo = new Image { Source = ImageSource.FromFile("icon.png") };

			myNav.Content = new StackLayout
			{
				Padding = new Thickness(20.0, 0.0),
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children =
					{
					btnBack,
					title,
					toprightlogo
}
			};
		}

	}
}
