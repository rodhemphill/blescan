﻿using System;
using System.Collections.Generic;
using BleScan.ViewModels;
using BluetoothLE.Core;

using Xamarin.Forms;

namespace BleScan.Views
{
	public partial class DevicesPage : ContentPage
	{
		private DevicesPageViewModel ViewModel;
		public DevicesPage()
		{
			InitializeComponent();
			ViewModel = (DevicesPageViewModel)BindingContext;
		}

		public async void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (e.Item == null)
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null

			await ((DevicesPageViewModel)this.BindingContext).OnItemSelectedCommand.Execute((IDevice)e.Item);

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.OnAppearing();
		}

	}
}
