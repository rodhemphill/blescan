﻿using System;
using System.Collections.Generic;
using BleScan.ViewModels;
using BluetoothLE.Core;

using Xamarin.Forms;

namespace BleScan.Views
{
	public partial class CharacteristicsPage : ContentPage
	{
		private CharacteristicsPageViewModel ViewModel;
		public CharacteristicsPage()
		{
			NavigationPage.SetHasNavigationBar(this, true);
			InitializeComponent();
			ViewModel = (CharacteristicsPageViewModel)BindingContext;
			NavigationPage.SetHasNavigationBar(this, true);
		}

		//public async void OnItemTapped(object sender, ItemTappedEventArgs e)
		//{
		//	if (e.Item == null)
		//		return;

		//	await ((CharacteristicsPageViewModel)this.BindingContext).OnItemSelectedCommand.Execute((ICharacteristic)e.Item);

		//}
	}
}
