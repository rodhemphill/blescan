﻿using System;
using Xamarin.Forms;

namespace BleScan.Converters
{
	public class SwitchColorConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if ((bool)value)
				return Color.Green;
			else
				return Color.Red;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if ((bool)value)
				return Color.Green;
			else
				return Color.Red;
		}
	}
}
