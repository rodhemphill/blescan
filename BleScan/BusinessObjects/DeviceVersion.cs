﻿using System;
namespace BleScan.BusinessObjects
{
	public enum DeviceVersion
	{
		PUMP_DEV_VER_1HP_AC,
		BLDC_1HP0_NOPFC_Australian_V1,
		BLDC_1HP5_NOPFC_Australian_V1,
		BLDC_2HP0_PFC_Australian_V1,
		BLDC_1HP0_PFC_Australian_V1,
		BLDC_1HP0_NOPFC_US_V1,
		BLDC_1HP5_NOPFC_US_V1,
		BLDC_2HP0_PFC_US_V1,
		BLDC_1HP0_PFC_US_V1,
		ACIM_1HP0_NOPFC_AU_V1,
		ACIM_2HP0_PFC_AU_V1,
		ACIM_1HP0_NOPFC_US_V1,
		ACIM_2HP0_PFC_US_V1
	}


}

