﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BleScan.BusinessObjects;
using Prism.Commands;
using Xamarin.Forms;

namespace BleScan.BusinessObjects
{
	public class AuthorisedPump
	{
		public string Name { get; set; }
		public string Status { get; set; }
		public string AccessCode { get; set; }
		public long UniqueId { get; set; }
		public bool IsEditMode { get; set; }
		public Color NameTextColor { get; set; }

		private DelegateCommand _deletePumpCommand;
		public DelegateCommand DeletePumpCommand => _deletePumpCommand != null ? _deletePumpCommand : (_deletePumpCommand = new DelegateCommand(DoDeletePumpCommand));
		private async void DoDeletePumpCommand()
		{

		}

	}
}
