﻿using System;
using BleScan.BusinessObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BluetoothLE.Core;
using System.Diagnostics;

namespace BleScan.BusinessObjectServices
{
	public class DeviceServices : IDeviceServices
	{
		
		public DeviceServices()
		{
		}

		#region Interface Methods
		private IDevice _currentDevice = null;
		public IDevice CurrentDevice
		{
			get
			{
				return _currentDevice;
			}

			set
			{
				if (value != _currentDevice)
					_currentDevice = value;
			}
		}

		#endregion
	}
}
