﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BleScan.BusinessObjects;
using BluetoothLE.Core;

namespace BleScan.BusinessObjectServices
{
	public interface IDeviceServices
	{
		IDevice CurrentDevice { get; set; }
	}
}
