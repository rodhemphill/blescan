﻿using BleScan.BusinessObjectServices;
using BleScan.Views;
using BluetoothLE.Core;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BleScan
{

	public partial class App : PrismApplication
	{
		static public int ScreenWidth;

		public App(IPlatformInitializer initializer = null) : base(initializer)
		{

		}

		private IAdapterServices _adapterServices;
		protected override void OnInitialized()
		{
			// Set the Bluetooth adapter
			_adapterServices = Container.Resolve<IAdapterServices>();
			_adapterServices.BluetoothAdapter = DependencyService.Get<IAdapter>();

			var navPage = new NavigationPage(new DevicesPage())
			{
				BarBackgroundColor = Color.Black,
				BarTextColor = Color.White
			};

			InitializeComponent();
			MainPage = navPage;

			NavigationService.NavigateAsync("DevicesPage");
		}

		protected override void RegisterTypes()
		{
			BleScan.Settings.Container = Container;
			BluetoothLE.Settings.Container = Container;

			Container.RegisterType<IAdapterServices, AdapterServices>(new ContainerControlledLifetimeManager());
			Container.RegisterType<IDeviceServices, DeviceServices>(new ContainerControlledLifetimeManager());

			Container.RegisterTypeForNavigation<DevicesPage>();
			Container.RegisterTypeForNavigation<ServicesPage>();
			Container.RegisterTypeForNavigation<CharacteristicsPage>();

		}

		public static void Cleanup()
		{
			var _deviceServices = Settings.Container.Resolve<IDeviceServices>();
			if (_deviceServices.CurrentDevice != null)
			{
				_deviceServices.CurrentDevice.Disconnect();
			}
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			Cleanup();
		}

		protected override void OnResume()
		{
		}
	}
}
