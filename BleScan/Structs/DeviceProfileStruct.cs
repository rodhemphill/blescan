﻿using System;
namespace BleScan.Structs
{
	public struct DeviceProfileStruct
	{
		public byte DeviceType;
		public byte DeviceVersion;
		public byte DeviceProtocol;
		public byte DeviceProtocolRevision;
		public UInt16 FirmwareVersion;
		public UInt32 SerialNumber;

		public char filler1;
		public char filler2;
		public char filler3;
		public char filler4;
		public char filler5;
		public char filler6;
		public char filler7;
		public char filler8;
		public char filler9;
		public char filler10;
	}
}
