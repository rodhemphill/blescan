﻿using System;
namespace BleScan.Structs
{
	public struct PumpCapabilitiesStruct
	{
		public UInt16 MinimumPumpSpeed;
		public UInt16 MaximumPumpSpeed;
		public char FullSpeedModeSupported;

		public char filler1;
		public char filler2;
		public char filler3;
		public char filler4;
		public char filler5;
		public char filler6;
		public char filler7;
		public char filler8;
		public char filler9;
		public char filler10;
		public char filler11;
		public char filler12;
		public char filler13;
		public char filler14;
		public char filler15;
	}
}
