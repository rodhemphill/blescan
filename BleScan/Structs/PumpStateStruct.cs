﻿using System;

namespace BleScan.Structs
{
	//[Serializable]
	public struct PumpStateStruct
	{
		public char PumpMode;
		public char PumpSpeedLevel;
		public char ActiveTimer;
		public char PumpWarnings;
		public char PumpFaults;
		public byte Flags;
		public UInt16 PumpSpeed;
		public char reserverd;
		public char TimeHours;
		public char TimeMinutes;
		public char TimeSeconds;
		public UInt16 PumpRealPowerWatts;
		public UInt16 PumpInputVoltageVolts;
		public UInt16 PumpMotorInputCurrentAmps;
		public UInt16 RollingNumber;
	}
}
