﻿using System;
namespace BleScan.Structs
{
	public struct DeviceTimeStruct
	{
		public char Seconds;
		public char Minutes;
		public char Hours;
		public char DayOfMonth;
		public char Month;
		public char reserved;
		public UInt16 Year;

		public char filler1;
		public char filler2;
		public char filler3;
		public char filler4;
		public char filler5;
		public char filler6;
		public char filler7;
		public char filler8;
		public char filler9;
		public char filler10;
		public char filler11;
		public char filler12;
	}
}
