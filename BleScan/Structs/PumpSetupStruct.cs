﻿using System;
namespace BleScan.Structs
{
	public struct PumpSetupStruct
	{
		public UInt16 LowSpeed;
		public UInt16 MediumSpeed;
		public UInt16 HighSpeed;
		public UInt16 PrimingSpeed;
		public UInt16 PrimingTimeSec;
		public byte TimersEnabled;
		public byte SystemModeEnabled;
		public char PumpId;

		public char filler1;
		public char filler2;
		public char filler3;
		public char filler4;
		public char filler5;
		public char filler6;
		public char filler7;
	}
}