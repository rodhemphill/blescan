﻿using System;
namespace BleScan.Structs
{
	public struct PumpTimersStruct
	{
		public char Timer1Packed;
		public char Timer1StartMinutes;
		public char Timer1StopHours;
		public char Timer1StopMinutes;

		public char Timer2Packed;
		public char Timer2StartMinutes;
		public char Timer2StopHours;
		public char Timer2StopMinutes;

		public char Timer3Packed;
		public char Timer3StartMinutes;
		public char Timer3StopHours;
		public char Timer3StopMinutes;

		public char Timer4Packed;
		public char Timer4StartMinutes;
		public char Timer4StopHours;
		public char Timer4StopMinutes;
	
		public char filler1;
		public char filler2;
		public char filler3;
		public char filler4;
	}
}
