﻿using System;
namespace BleScan.Structs
{
	public struct ScanResponseStruct
	{
		public byte ManufacturerIdLo;
		public byte ManufacturerIdHi;
		public byte DeviceType;
		public byte DeviceVersion;
		public byte DeviceProtocol;
		public byte DeviceProtocolRevision;
		public byte DeviceStatus;
		public byte Reserverd;
		public unsafe fixed byte DeviceUniqueId[8];	 

	}
}