﻿using System;
using System.Collections.Generic;
using BleScan.BusinessObjects;
using BluetoothLE.Core;
using Microsoft.Practices.Unity;

namespace BleScan
{
	public enum TemplateType
	{
		Jaccuzi,
		BleScan
	}

	public static class Settings
	{
		public static string AppVersion = "0.0.1";
		public static bool DemoModeAllowed = true;
		public static bool DebugMode = true;

		public static int DeviceConnectionTimeoutMs = 7000;   // Milliseconds waiting for successful connection



		public static TemplateType TemplateType = TemplateType.Jaccuzi;

		public static IUnityContainer Container;
		public static string BrandColor = "#fcbd36";
		public static string BaseTextColor = "#ffffff";

		public static int PumpSpeedIncrements = 25;
		public static int PumpSpeedEditStaysVisible = 10000; // Milliseconds the plus minus buttons stay visible on the pump screen
		public static int PumpConnectionTimeoutMs = 7000;	// Milliseconds waiting for successful pump connection

		public static int[] PrimingTimeValidValues = { 2, 5, 10, 15, 20, 30, 45, 60, 90, 120 };
		public static int MaxPrimingTimeMin = 120;
		public static int MinPrimingTimeMin = 2;

		public static int MaxPrimingSpeed = 3100;
		public static int MinPrimingSpeed = 600;
		public static double LargePrimingSpeedStepValue = 250;
		public static double SmallPrimingSpeedStepValue = 25;

		public static int MaxMinutesDiscrepancy = 3;	// if Pump time out by more than this, auto set the time to phone time.

		public static string SecretKey = "2b7e151628aed2a6abf7158809cf4f3c";
	}
}
