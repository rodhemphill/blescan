﻿using System;
using Xamarin.Forms;

namespace BleScan.CustomControls
{
	public class LongPressButton : Button
	{
		public event EventHandler PressStarted;
		public event EventHandler PressEnded;

		public void OnPressStarted()
		{
			if (PressStarted != null)
			{
				PressStarted(this, EventArgs.Empty);
			}
		}

		public void OnPressEnded()
		{
			if (PressEnded != null)
			{
				PressEnded(this, EventArgs.Empty);
			}
		}
	}
}
