﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using BluetoothLE.Core;
using BluetoothLE.Core.Events;
using BleScan.BusinessObjectServices;

namespace BleScan.ViewModels
{
	public class CharacteristicsPageViewModel : BindableBase, INavigationAware
	{
		#region private
		private IService connectedService = null;
		private IAdapter adapter;
		#endregion
		#region UIProperties
		string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}

		ObservableCollection<ICharacteristic> _devices = new ObservableCollection<ICharacteristic>();
		public ObservableCollection<ICharacteristic> Characteristics
		{
			get
			{
				return _devices;
			}
			set
			{
				SetProperty(ref _devices, value);
			}
		}
		#endregion
		#region ICommands
		#endregion
		#region private methods

		#endregion
		#region Constructors
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
			if (parameters.ContainsKey("service"))
			{
				connectedService = (IService)parameters["service"];
				Characteristics = connectedService.Characteristics;

				if (connectedService == null)
					throw new Exception("EditTimerPageViewModel: Opps ... no timer passed");

			}
		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{
		}

		private readonly IDeviceServices _deviceServices;
		private readonly INavigationService _navigationService;
		public CharacteristicsPageViewModel(
			IDeviceServices deviceServices,
			INavigationService navigationService
		)
		{
			_deviceServices = deviceServices;
			_navigationService = navigationService;
			adapter = DependencyService.Get<IAdapter>();
		}
		#endregion

	}
}
