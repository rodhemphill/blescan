﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using BluetoothLE.Core;
using BluetoothLE.Core.Events;
using BleScan.BusinessObjectServices;

namespace BleScan.ViewModels
{
	public class ServicesPageViewModel : BindableBase
	{
		#region private
		private IService connectedService = null;
		private IAdapter adapter;
		private int testServiceIndex;
		#endregion
		#region UIProperties
		string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}

		ObservableCollection<IService> _devices = new ObservableCollection<IService>();
		public ObservableCollection<IService> Services
		{
			get
			{
				return _devices;
			}
			set
			{
				SetProperty(ref _devices, value);
			}
		}
		#endregion
		#region ICommands
		private DelegateCommand<IService> _onItemSelectedCommand;
		public DelegateCommand<IService> OnItemSelectedCommand => _onItemSelectedCommand != null ? _onItemSelectedCommand : (_onItemSelectedCommand = new DelegateCommand<IService>(DoOnItemSelectedCommand));
		private async void DoOnItemSelectedCommand(IService service)
		{
			ErrorMessage = "";

			var p = new NavigationParameters();
			p.Add("service", service);
			await _navigationService.NavigateAsync("CharacteristicsPage", p, false, true);
		}

		#endregion
		#region private methods

  		#endregion
		#region Constructors
		private readonly IDeviceServices _deviceServices;
		private readonly INavigationService _navigationService;
		public ServicesPageViewModel(
			IDeviceServices deviceServices,
			INavigationService navigationService
		)
		{
			_deviceServices = deviceServices;
			_navigationService = navigationService;
			adapter = DependencyService.Get<IAdapter>();

			Services = _deviceServices.CurrentDevice.Services;

		}
		#endregion

	}
}
