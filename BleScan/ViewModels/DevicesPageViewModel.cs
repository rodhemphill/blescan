﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using BluetoothLE.Core;
using BluetoothLE.Core.Events;
using BleScan.BusinessObjectServices;

namespace BleScan.ViewModels
{
	public class DevicesPageViewModel : BindableBase
	{
		#region private
		private IAdapter adapter;
		private int testDeviceIndex;
		bool connected = false;
		#endregion
		#region UIProperties
		string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}

		ObservableCollection<IDevice> _devices = new ObservableCollection<IDevice>();
		public ObservableCollection<IDevice> Devices
		{
			get
			{
				return _devices;
			}
			set
			{
				SetProperty(ref _devices, value);
			}
		}
		#endregion
		#region ICommands
		private DelegateCommand<IDevice> _onItemSelectedCommand;
		public DelegateCommand<IDevice> OnItemSelectedCommand => _onItemSelectedCommand != null ? _onItemSelectedCommand : (_onItemSelectedCommand = new DelegateCommand<IDevice>(DoOnItemSelectedCommand));
		private async void DoOnItemSelectedCommand(IDevice device)
		{
			ErrorMessage = "";
			connected = false;
			adapter.DeviceConnected += OnDeviceConnected;
			adapter.ConnectToDevice(device);

			await Task.Delay(Settings.DeviceConnectionTimeoutMs);

			adapter.DeviceConnected -= OnDeviceConnected;
			if (!connected)
			{
				ErrorMessage = "Timed out connecting to device";
			}
		}

		private DelegateCommand _scanCommand;
		public DelegateCommand ScanCommand => _scanCommand != null ? _scanCommand : (_scanCommand = new DelegateCommand(DoScanCommand));
		private void DoScanCommand()
		{
			ErrorMessage = "";
			Devices = adapter.DiscoveredDevices;
			adapter.StopScanningForDevices();
			adapter.StartScanningForDevices();
		}

		private DelegateCommand _testCommand;
		public DelegateCommand TestCommand => _testCommand != null ? _testCommand : (_testCommand = new DelegateCommand(DoTestCommand));
		private void DoTestCommand()
		{
			if (Devices.Count == 0)
			{
				ErrorMessage = "There are no devices to test";
				return;
			}

			ErrorMessage = "";
			testDeviceIndex = -1;
			TestNextDevice();
		}
		private async void TestNextDevice()
		{
			testDeviceIndex++;
			if (testDeviceIndex >= Devices.Count)
				return;

			var device = Devices[testDeviceIndex];
			adapter.DeviceConnected += OnTestDeviceConnected;
			adapter.ConnectToDevice(device);

			await Task.Delay(Settings.DeviceConnectionTimeoutMs);

			adapter.DeviceConnected -= OnTestDeviceConnected;
			if (device.State != DeviceState.Connected)
			{
				TestNextDevice();
			}

		}
		private void OnTestDeviceConnected(object sender, DeviceConnectionEventArgs e)
		{
			var i = Devices.IndexOf(e.Device);
			Devices.RemoveAt(i);
			Devices.Insert(i, e.Device);
			adapter.DisconnectDevice(e.Device);
			TestNextDevice();
		}

		#endregion
		#region Methods
		private async void OnDeviceConnected(object sender, DeviceConnectionEventArgs e)
		{
			connected = true;
			_deviceServices.CurrentDevice = e.Device;
			_deviceServices.CurrentDevice.ServiceDiscovered += this.OnServiceDiscovered;
			_deviceServices.CurrentDevice.DiscoverServices();

			await _navigationService.NavigateAsync("ServicesPage", null, false, true);
		}
		private void OnServiceDiscovered(object sender, ServiceDiscoveredEventArgs e)
		{
			e.Service.DiscoverCharacteristics();
		}

		/// <summary>
		/// Triggered from code behind when page appears
		/// </summary>
		public void OnAppearing()
		{
			if (_deviceServices.CurrentDevice != null)
			{
				adapter.DisconnectDevice(_deviceServices.CurrentDevice);
				_deviceServices.CurrentDevice.Dispose();
				_deviceServices.CurrentDevice = null;
			}

			Devices = adapter.DiscoveredDevices;
			adapter.StopScanningForDevices();
			adapter.StartScanningForDevices();

		}
		#endregion
		#region Constructors
		private readonly IDeviceServices _deviceServices;
		private readonly INavigationService _navigationService;
		public DevicesPageViewModel(
			IDeviceServices deviceServices,
			INavigationService navigationService
		)
		{
			_deviceServices = deviceServices;
			_navigationService = navigationService;
			adapter = DependencyService.Get<IAdapter>();
		}
		#endregion

	}
}
