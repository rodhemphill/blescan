﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Android.Bluetooth;

namespace BluetoothLE.Droid
{
	public enum CommandQueueAction
	{
		Read,
		Write,
		Notify,
		StopNotify
	}


	public class AndroidBluetoothCommandQueue
	{
		private static int queueStuckMs = 15000;		// next in queue is older than this
		private static int heavyProcessingMs = 3000;	// number of ms since last pump message 
		private static int heavyProcessingCount = 30;	// if the queue size is greater than this and response time is greater than the above, processor could be overloaded

		private bool _heavyProcessing = false;
		private bool heavyProcessing
		{
			get
			{
				if (fifoQueue.Count < 2)
					_heavyProcessing = false;

				if (_heavyProcessing)
					return true;

				// Can be older than the queueStuckMs if the proceessor can't keep up with the number of acctions.
				// Therefore only check if there hasn't been anything returned from the device since heavyProcessingMs.
				if (   lastResponseFromPump != DateTime.MinValue 
				    && (DateTime.Now - lastResponseFromPump).TotalMilliseconds < heavyProcessingMs
					&& fifoQueue.Count > heavyProcessingCount
					)
					_heavyProcessing = true;

				return _heavyProcessing;
			}

			set
			{
				_heavyProcessing = value;
			}
		}

		private DateTime lastResponseFromPump = DateTime.MinValue;

		private CommandQueueAction lastScrappedCommand;
		private string lastScrappedUuid;

		private ConcurrentQueue<Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]>> fifoQueue = new ConcurrentQueue<Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]>>();

		#region Constructor (sort of)
		static private AndroidBluetoothCommandQueue queue = null;
		static public AndroidBluetoothCommandQueue Queue
		{
			get
			{
				if (queue == null)
				{
					Debug.WriteLine("{0}  Creating new command queue");
					queue = new AndroidBluetoothCommandQueue();
				}
				return queue;
			}
		}
		#endregion
		#region Methods
		public void Enqueue(BluetoothGatt _gatt, CommandQueueAction action, BluetoothGattCharacteristic _nativeCharacteristic, byte[] writeData = null)
		{
			Debug.WriteLine("{0}  Add to queue ({3} queue count: {2})\t\t\t\t\t\t\t\t\tfor {1} ", DateTime.Now.ToString("hh:mm:ss.fff tt"),
							_nativeCharacteristic.Uuid.ToString().Substring(0, 8),  fifoQueue.Count, action);

			this.CheckIfQueueStuck();
			fifoQueue.Enqueue(new Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]>(DateTime.Now, action, _nativeCharacteristic, writeData));

			if (fifoQueue.Count == 1)
			{
				this.ExecuteNextCommand(_gatt);
			}

			this.dumpQueue();
		}

		public void Dequeue(BluetoothGatt _gatt,BluetoothGattCharacteristic lastCharacteristicShouldBe)
		{
			lastResponseFromPump = DateTime.Now;

			Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]> thisItem;
			if (!fifoQueue.TryDequeue(out thisItem))
				throw new Exception("Can't dequeue from CharacteristicThreadReadQueue");

			if (lastCharacteristicShouldBe != null && thisItem.Item3.Uuid.ToString() != lastCharacteristicShouldBe.Uuid.ToString())
			{
				if (thisItem.Item2 == lastScrappedCommand && thisItem.Item3.Uuid.ToString() == lastScrappedUuid)
				{
					Debug.WriteLine("{0}  Woops .. got command that was scrapped {1} {2} (processing and ignoring scrapping)", DateTime.Now.ToString("hh:mm:ss.fff tt"),
									thisItem.Item2, thisItem.Item3.Uuid);
					this.ExecuteCommand(_gatt, thisItem.Item2, thisItem.Item3, thisItem.Item4);
					return;
				}

				var message = String.Format("Dequeue out of sequence: {0} when expected {1}", thisItem.Item3.Uuid.ToString(), lastCharacteristicShouldBe.Uuid);
				Debug.WriteLine(message);
				throw new Exception(message);
			}

			Debug.WriteLine("{0}  Removed from queue (queue count: {2})\t\t\t\tfor {1}", DateTime.Now.ToString("hh:mm:ss.fff tt"),
				thisItem.Item3.Uuid.ToString().Substring(0, 8), fifoQueue.Count, thisItem.Item1);

			this.ExecuteNextCommand(_gatt);

			this.dumpQueue();
		}

		public void Clear()
		{
			fifoQueue = new ConcurrentQueue<Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]>>();
			heavyProcessing = false;
		}

		#endregion
		#region Private Methods
		private void dumpQueue()
		{
			if (!Settings.BluetoothLEDebugMode)
				return;
			
			// first one in the queue is the next one executed
			var row = 0;
			foreach (var item in fifoQueue)
			{
				Debug.WriteLine("\t\t\t\t\t\t\t\t{2} {0} {1} {3}", item.Item2, item.Item3.Uuid.ToString().Substring(0, 8), row,item.Item1.ToString("hh:mm:ss.fff tt"));
				row++;
			}
		}
		private void CheckIfQueueStuck()
		{
			if (this.heavyProcessing)
				return;

			while ("1" == "1")
			{
				if (fifoQueue.Count == 0)
					return;

				Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]> nextItem;
				if (!fifoQueue.TryPeek(out nextItem))
					return;
				
				var timeQueued = nextItem.Item1;
				if ((DateTime.Now - timeQueued).TotalMilliseconds < queueStuckMs)
					return;

				Debug.WriteLine("{0}  Scrapping command {1} {2} because older than {3}ms", DateTime.Now.ToString("hh:mm:ss.fff tt"),
								nextItem.Item2, nextItem.Item3.Uuid, queueStuckMs);
				lastScrappedCommand = nextItem.Item2;
				lastScrappedUuid = nextItem.Item3.Uuid.ToString();
				this.Dequeue();

				this.dumpQueue();
			}
		}

		/// <summary>
		/// Executes the next command (no dequeue).
		/// </summary>
		/// <param name="_gatt">Gatt.</param>
		private void ExecuteNextCommand(BluetoothGatt _gatt)
		{
			if (fifoQueue.Count == 0)
				return;

			Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]> nextItem;
			if (!fifoQueue.TryPeek(out nextItem))
				throw new Exception("Can't dequeue from CharacteristicThreadReadQueue");

			this.ExecuteCommand(_gatt, nextItem.Item2, nextItem.Item3, nextItem.Item4);
		}

		private void ExecuteCommand(BluetoothGatt _gatt, CommandQueueAction command, BluetoothGattCharacteristic characteristic, byte[] writeData)
		{
			Debug.WriteLine("{0}  Executing command {1} {2} (queue count: {3})", DateTime.Now.ToString("hh:mm:ss.fff tt"),command,characteristic.Uuid, fifoQueue.Count);
			if (fifoQueue.IsEmpty)
				return;

			switch (command)
			{
				case CommandQueueAction.Read:
					Debug.WriteLine("{0}  Read Characteristic (queue count: {4})\t\t\t\t\t\t\tfor {1} ({2}) Handle: {3}", DateTime.Now.ToString("hh:mm:ss.fff tt"),
									characteristic.Uuid.ToString().Substring(0, 8), "", _gatt.Handle, fifoQueue.Count);
					_gatt.ReadCharacteristic(characteristic);
					break;
				case CommandQueueAction.Write:
					characteristic.SetValue(writeData);
					characteristic.WriteType = GattWriteType.Default;

					Debug.WriteLine("{0}  Write Characteristic (queue count: {4})\t\t\t\t\t\t\tfor {1} ({2}) Handle: {3}", DateTime.Now.ToString("hh:mm:ss.fff tt"),
									characteristic.Uuid.ToString().Substring(0, 8), "", _gatt.Handle, fifoQueue.Count);
					_gatt.WriteCharacteristic(characteristic);
					break;
				case CommandQueueAction.Notify:
					this.SetNotify(_gatt, characteristic, true);
					break;
				case CommandQueueAction.StopNotify:
					this.SetNotify(_gatt, characteristic, false);
					break;
			}
		}

		private void Dequeue()
		{
			if (fifoQueue.IsEmpty)
			{
				Debug.WriteLine("WARNING: Trying to dequeue empty queue");
				return;
			}

			Tuple<DateTime, CommandQueueAction, BluetoothGattCharacteristic, byte[]> thisItem;
			if (!fifoQueue.TryDequeue(out thisItem))
				throw new Exception("Can't dequeue from CharacteristicThreadReadQueue");

			Debug.WriteLine("{0}  Removed from queue (queue count: {2})\t\t\t\tfor {1}", DateTime.Now.ToString("hh:mm:ss.fff tt"),
				thisItem.Item3.Uuid.ToString().Substring(0, 8), fifoQueue.Count, thisItem.Item1);
		}


		private async void SetNotify(BluetoothGatt _gatt,BluetoothGattCharacteristic characteristic,bool enable)
		{
			if (!_gatt.SetCharacteristicNotification(characteristic, enable))
				throw new Exception("Unable to set the notification value on the characteristic");
			Debug.WriteLine("{0}  Set Notify to true \t\t\t\t\t\t\t\t\tfor {1} ({2}) Handle: {3}", DateTime.Now.ToString("hh:mm:ss.fff tt"), characteristic.Uuid.ToString().Substring(0, 8), "", _gatt.Handle);

			// Needs some time before descriptor available
			await Task.Delay(100);

			if (characteristic.Descriptors.Count > 0)
			{
				const string descriptorId = "00002902-0000-1000-8000-00805f9b34fb";
				var descriptor = characteristic.Descriptors.FirstOrDefault(x => x.Uuid.ToString() == descriptorId);
				if (descriptor == null)
					throw new Exception("Descriptor 2902 not found");

				var value = enable ? BluetoothGattDescriptor.EnableNotificationValue : BluetoothGattDescriptor.DisableNotificationValue;
				var result = descriptor.SetValue(value.ToArray());
				if (!result)
					throw new Exception("Unable to set the notification value on the descriptor");

				try
				{
					await Task.Delay(100);
					result = _gatt.WriteDescriptor(descriptor);
					if (!result)
					{
						await Task.Delay(100);
						result = _gatt.WriteDescriptor(descriptor);
						if (!result)
						{
							this.Dequeue(_gatt, characteristic);
							throw new Exception("Failed to rewrite the descriptor");
						}
					}
				}
				catch (Exception e)
				{
					Debug.WriteLine(e);
				}
			}
		}
		#endregion

	}
}
