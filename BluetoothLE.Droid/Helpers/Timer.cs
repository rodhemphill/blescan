﻿using System.Threading;
using System.Threading.Tasks;
using System;

internal delegate void TimerCallback (object state);

namespace Helpers
{
	class TimerState {
		public int counter = 0;
		public Timer tmr;
	}

	internal sealed class Timer : CancellationTokenSource, IDisposable
	{
		internal Timer (TimerCallback callback, object state, int dueTime, int period)
		{
			Task.Delay (dueTime, Token).ContinueWith ((t, s) => {
				var tuple = (Tuple<TimerCallback, object>)s;
				tuple.Item1 (tuple.Item2);
				if (period != 0){
					recurseRun(callback, state, period);
				}
			}, Tuple.Create (callback, state), CancellationToken.None,
				TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnRanToCompletion,
				TaskScheduler.Default);


		}

		private void recurseRun(TimerCallback callback, object state, int period)
		{
			Task.Delay (period, Token).ContinueWith ((t, s) => {
				
				var tuple = (Tuple<TimerCallback, object>)s;
				tuple.Item1 (tuple.Item2);
				recurseRun(callback, state, period);

			}, Tuple.Create (callback, state), CancellationToken.None,
				TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnRanToCompletion,
				TaskScheduler.Default);
		}


		public new void Dispose ()
		{
			base.Cancel ();
		}
	} 
}