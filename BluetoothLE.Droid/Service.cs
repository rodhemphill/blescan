﻿using System;
using BluetoothLE.Core;
using Android.Bluetooth;
using Java.Util;
using System.Collections.Generic;
using BluetoothLE.Core.Events;
using Microsoft.Practices.Unity;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace BluetoothLE.Droid
{
	/// <summary>
	/// Concrete implmentation of <see cref="BluetoothLE.Core.IService" /> interface
	/// </summary>
	public class Service : IService, IDisposable
	{
		public string InstanceId { get; set; }
		private readonly BluetoothGattService _nativeService;
		private readonly BluetoothGatt _gatt;
		private readonly GattCallback _callback;
		/// <summary>
		/// Initializes a new instance of the <see cref="BluetoothLE.Droid.Service"/> class.
		/// </summary>
		/// <param name="nativeService">Native service.</param>
		/// <param name="gatt">Native Gatt.</param>
		/// <param name="callback">Callback.</param>
		public Service(BluetoothGattService nativeService, BluetoothGatt gatt, GattCallback callback)
		{
			_nativeService = nativeService;
			_gatt = gatt;
			_callback = callback;

			_id = ServiceIdFromUuid(_nativeService.Uuid);

			InstanceId = String.Format("S{0}", DateTime.Now.ToString("mmss.fff"));
			Debug.WriteLine("{0}  New Service Created\t\t\t\t\t\t\tfor {1} ({2}) Gatt Handle: {3}, gatt callback: {4} ({5})", DateTime.Now.ToString("hh:mm:ss.fff tt"),
			                _nativeService.Uuid.ToString().Substring(0, 8), InstanceId, gatt.Handle, callback.Handle, callback.InstanceId);

			Characteristics = new ObservableCollection<ICharacteristic>();
		}

		/// <summary>
		/// Gets a service identifier from a UUID
		/// </summary>
		/// <returns>The service identifier.</returns>
		/// <param name="uuid">The service UUID.</param>
		public static Guid ServiceIdFromUuid(UUID uuid)
		{
			return Guid.ParseExact(uuid.ToString(), "d");
		}

		#region IService implementation

		/// <summary>
		/// Occurs when characteristics discovered.
		/// </summary>
		public event EventHandler<CharacteristicDiscoveredEventArgs> CharacteristicDiscovered = delegate {};

		/// <summary>
		/// Discovers the characteristics for the services.
		/// </summary>
		public void DiscoverCharacteristics()
		{
			Characteristics = new ObservableCollection<ICharacteristic>();
			// already have the characteristics
			foreach (var c in _nativeService.Characteristics)
			{
				if (c.Uuid.ToString() == "45000100-98b7-4e29-a03f-160174643001")
					Debug.WriteLine("{0}  ABOUT to create Characteristic\t\t\t\t\tfor {1} ({2}) gatt handle: {3} gatt callback: {4} ({5})", DateTime.Now.ToString("hh:mm:ss.fff tt"), 
					                c.Uuid.ToString().Substring(0, 8), InstanceId,_gatt.Handle, _callback.Handle, _callback.InstanceId);
					
				var characteristic = new Characteristic(c, _gatt, _callback);
				Characteristics.Add(characteristic);
				CharacteristicDiscovered(this, new CharacteristicDiscoveredEventArgs(characteristic));
			}
		}

		private readonly Guid _id;
		/// <summary>
		/// Gets the service's unique identifier.
		/// </summary>
		/// <value>The identifier.</value>
		public Guid Id { get { return _id; } }

		/// <summary>
		/// Gets the UUID.
		/// </summary>
		/// <value>The UUID.</value>
		public string Uuid { get { return _nativeService.Uuid.ToString(); }}

		/// <summary>
		/// Gets a value indicating whether this instance is primary.
		/// </summary>
		/// <value>true</value>
		/// <c>false</c>
		public bool IsPrimary { get { return _nativeService.Type == GattServiceType.Primary; } }

		/// <summary>
		/// Gets the service's characteristics.
		/// </summary>
		/// <value>The characteristics.</value>
		public ObservableCollection<ICharacteristic> Characteristics { get; set; }

		#endregion
		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			Debug.WriteLine("{0}  --- Disposing Service \t\t\t\t\t\t\t\t\tfor {1} ({2})", DateTime.Now.ToString("hh:mm:ss.fff tt"), Uuid.ToString().Substring(0, 8),InstanceId);
			if (!disposedValue)
			{
				if (disposing)
				{
					if (Characteristics.Count == 0)
						Debug.WriteLine("{0}       --- Characteristics == 0");
					
					foreach (var characteristic in Characteristics)
					{
						characteristic.Dispose();
					}
					Characteristics = null;
				}

				disposedValue = true;
			}
		}

		 ~Service() {
		   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		   Dispose(false);
		 }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			 GC.SuppressFinalize(this);
		}
		#endregion

	}
}

