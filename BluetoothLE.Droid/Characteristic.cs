﻿using System;
using BluetoothLE.Core;
using Android.Bluetooth;
using System.Linq;
using BluetoothLE.Core.Events;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Helpers;

namespace BluetoothLE.Droid
{
	/// <summary>
	/// Concrete implmentation of <see cref="BluetoothLE.Core.ICharacteristic" /> interface
	/// </summary>
	public class Characteristic : ICharacteristic, IDisposable
	{
		public string InstanceId { get; set; }
		private readonly BluetoothGattCharacteristic _nativeCharacteristic;
		private readonly BluetoothGatt _gatt;
		private readonly GattCallback _callback;

		TimerState batchTimer;
		bool timerRunning = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="BluetoothLE.Droid.Characteristic"/> class.
		/// </summary>
		/// <param name="characteristic">Characteristic.</param>
		/// <param name="gatt">Gatt.</param>
		/// <param name="gattCallback">Gatt callback.</param>
		public Characteristic(BluetoothGattCharacteristic characteristic, BluetoothGatt gatt, GattCallback gattCallback)
		{
			_nativeCharacteristic = characteristic;
			_gatt = gatt;
			_callback = gattCallback;

 			InstanceId = String.Format("C{0}", DateTime.Now.ToString("mmss.fff"));
			Debug.WriteLine("{0}  New Characteristic Created\t\t\t\t\t\t\t\tfor {1} ({2}) Gatt Handle: {3} gatt fallback: {4} ({5})", DateTime.Now.ToString("hh:mm:ss.fff tt"), 
			                characteristic.Uuid.ToString().Substring(0, 8), InstanceId,_gatt.Handle, _callback.Handle,_callback.InstanceId);

			_callback.CharacteristicValueUpdated += CharacteristicValueUpdated;
		}

		#region ICharacteristic implementation

		/// <summary>
		/// Occurs when value updated.
		/// </summary>
		public event EventHandler<CharacteristicReadEventArgs> ValueUpdated = delegate {};

		/// <summary>
		/// Subscribe to the characteristic
		/// </summary>
		public void StartUpdates()
		{
			if (!CanUpdate)
				throw new InvalidOperationException("Characteristic does not support UPDATE");

			//_callback.CharacteristicValueUpdated -= CharacteristicValueUpdated;
			_callback.CharacteristicValueUpdated += CharacteristicValueUpdated;
			this.SetUpdateValue(true);
		}

		/// <summary>
		/// Unsubscribe from the characteristic
		/// </summary>
		public void StopUpdates()
		{
			if (CanUpdate)
			{
				_callback.CharacteristicValueUpdated -= CharacteristicValueUpdated;
				this.SetUpdateValue(false);
			}
		}

		/// <summary>
		/// Read the characteristic's value
		/// </summary>
		public void Read()
		{
			if (!CanRead)
				throw new InvalidOperationException("Characteristic does not support READ");

			AndroidBluetoothCommandQueue.Queue.Enqueue(_gatt, CommandQueueAction.Read, _nativeCharacteristic);
		}

		/// <summary>
		/// Write the specified data to the characteristic
		/// </summary>
		/// <param name="data">Data.</param>
		public void Write(byte[] data)
		{
			if (!CanWrite)
				throw new InvalidOperationException("Characteristic does not support WRITE");

			AndroidBluetoothCommandQueue.Queue.Enqueue(_gatt, CommandQueueAction.Write, _nativeCharacteristic, data);
		}

		/// <summary>
		/// Gets the unique identifier.
		/// </summary>
		/// <value>The unique identifier.</value>
		public Guid Id { get { return Guid.Parse(_nativeCharacteristic.Uuid.ToString()); } }

		/// <summary>
		/// Gets the UUID.
		/// </summary>
		/// <value>The UUID.</value>
		public string Uuid { get { return _nativeCharacteristic.Uuid.ToString(); } }

		/// <summary>
		/// Gets the characteristic's value.
		/// </summary>
		/// <value>The characteristic's value.</value>
		public byte[] Value { get { return _nativeCharacteristic.GetValue(); } }

		/// <summary>
		/// Gets the characteristic's value as a string.
		/// </summary>
		/// <value>The characteristic's value, interpreted as a string.</value>
		public string StringValue
		{
			get
			{
				var val = Value;
				if (val == null)
					return string.Empty;

				return System.Text.Encoding.UTF8.GetString(val);
			}
		}

		/// <summary>
		/// Gets the native characteristic. Should be cast to the appropriate type.
		/// </summary>
		/// <value>The native characteristic.</value>
		public object NativeCharacteristic { get { return _nativeCharacteristic; } }

		/// <summary>
		/// Gets the characteristic's properties
		/// </summary>
		/// <value>The characteristic's properties.</value>
		public CharacteristicPropertyType Properties { get { return (CharacteristicPropertyType)(int)_nativeCharacteristic.Properties; } }

		/// <summary>
		/// Gets a value indicating whether this instance can read.
		/// </summary>
		/// <value>true</value>
		/// <c>false</c>
		public bool CanRead { get { return (Properties & CharacteristicPropertyType.Read) > 0; } }

		/// <summary>
		/// Gets a value indicating whether this instance can update.
		/// </summary>
		/// <value>true</value>
		/// <c>false</c>
		public bool CanUpdate { get { return (Properties & CharacteristicPropertyType.Notify) > 0; } }

		/// <summary>
		/// Gets a value indicating whether this instance can write.
		/// </summary>
		/// <value>true</value>
		/// <c>false</c>
		public bool CanWrite { get { return (Properties & (CharacteristicPropertyType.WriteWithoutResponse | CharacteristicPropertyType.AppleWriteWithoutResponse)) > 0; } }

		#endregion

		#region GattCallback delegate methods

		private void CharacteristicValueUpdated (object sender, CharacteristicReadEventArgs e)
		{
			if (e.Characteristic.Id == this.Id)
			{
				Debug.WriteLine("{0}           gatt callback: CharacteristicValueUpdated detected Id = {1}\t\t\tfor {3} ({4}) and {5} ({6})", DateTime.Now.ToString("hh:mm:ss.fff tt"),
								e.Characteristic.Id, this.Id, e.Characteristic.Uuid.ToString().Substring(0, 8), e.Characteristic.InstanceId, this.Uuid.ToString().Substring(0, 8), this.InstanceId);
				ValueUpdated(this, e);
			}
		}


		#endregion

		private void SetUpdateValue(bool enable)
		{
			if (enable)
				AndroidBluetoothCommandQueue.Queue.Enqueue(_gatt, CommandQueueAction.Notify, _nativeCharacteristic);
			else
				AndroidBluetoothCommandQueue.Queue.Enqueue(_gatt, CommandQueueAction.StopNotify, _nativeCharacteristic);

		}

		#region Batch Processing
		private void StartBatchProcessiong()
		{
			batchTimer = new TimerState();
			TimerCallback timerDelegate = new TimerCallback(ThreadedBatchProcess);
			Helpers.Timer timer = new Helpers.Timer(timerDelegate, batchTimer, 3000, 500);
			batchTimer.tmr = timer;  // So we have a handle to dispose
		}

		private void StopBatchProcessiong()
		{
			batchTimer.tmr.Cancel();
			batchTimer.tmr.Dispose();
			batchTimer = null;
		}

		private void ThreadedBatchProcess(Object state)
		{
			this.Read();
		}
		#endregion
		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			Debug.WriteLine("{0}  --- Disposing Characteristic \t\t\t\t\t\t\t\tfor {1} ({2}) Handle: {3}", DateTime.Now.ToString("hh:mm:ss.fff tt"), _nativeCharacteristic.Uuid.ToString().Substring(0, 8),InstanceId,_gatt.Handle);
			if (!disposedValue)
			{
				if (disposing)
				{
				}

				if (_callback != null)
					_callback.CharacteristicValueUpdated -= CharacteristicValueUpdated;

				if (timerRunning)
					this.StopBatchProcessiong();

				disposedValue = true;
			}
		}

		 ~Characteristic() {
		   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		   Dispose(false);
		 }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			 GC.SuppressFinalize(this);
		}
		#endregion
	}
}

