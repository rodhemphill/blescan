﻿using System;
using BluetoothLE.Core;
using Android.Bluetooth;
using System.Collections.Generic;
using System.Linq;
using BluetoothLE.Core.Events;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace BluetoothLE.Droid
{

    /// <summary>
    /// Concrete implmentation of <see cref="BluetoothLE.Core.IDevice" /> interface
    /// </summary>
	public class Device : IDevice, IDisposable
    {
        private readonly BluetoothDevice _nativeDevice;
 		public string InstanceId { get; set;} 
       
		private  BluetoothGatt _gatt;
		public BluetoothGatt Gatt { 
			get
			{
				return _gatt;
			} 
			set
			{
				_gatt = value;
				//_gatt.Connect();
				Debug.WriteLine("{0}  Adding gatt to device\t\t\t\t\t\t\tfor {1} ({2}) Gatt Handle: {3}", DateTime.Now.ToString("hh:mm:ss.fff tt"), this.Name, InstanceId,_gatt.Handle);

			}
		}

		private  GattCallback _callback;
		public GattCallback Callback
		{
			get { return _callback;}
			set {
				_callback = value;
				_callback.ServicesDiscovered -= ServicesDiscovered;
				_callback.ServicesDiscovered += ServicesDiscovered;
				Debug.WriteLine("{0}  Adding gatt callback and service event handler\t\t\t\t\t\t\tfor {1} ({2}) callback handle: {3} ({4})", DateTime.Now.ToString("hh:mm:ss.fff tt"), 
				                this.Name, InstanceId,_callback.Handle,_callback.InstanceId);
			}
		}


        private readonly byte[] _scanRecord;

        /// <summary>
        /// Initializes a new instance of the <see cref="BluetoothLE.Droid.Device"/> class.
        /// </summary>
        /// <param name="nativeDevice">Native device.</param>
        /// <param name="gatt">Native Gatt.</param>
        /// <param name="callback">Callback.</param>
        /// <param name="rssi">Rssi.</param>
        /// <param name="scanRecord">scanRecord.</param>
        public Device(BluetoothDevice nativeDevice, BluetoothGatt gatt, GattCallback callback, int rssi, byte[] scanRecord)
        {
			InstanceId = String.Format("D{0}", DateTime.Now.ToString("mmss.fff"));
			Debug.WriteLine("{0}  New Device Created (gatt is null)\t\t\t\tfor {1} ({2}) [{3}]", DateTime.Now.ToString("hh:mm:ss.fff tt"),nativeDevice.Name, InstanceId, nativeDevice.Address);
            _nativeDevice = nativeDevice;
            _gatt = gatt;
            _callback = callback;
            _scanRecord = scanRecord;
            _rssi = rssi;
            _id = DeviceIdFromAddress(_nativeDevice.Address);

            Services = new ObservableCollection<IService>();
        }

        /// <summary>
        /// Gets a device identifier from a MAC address
        /// </summary>
        /// <returns>The device identifier.</returns>
        /// <param name="address">The MAC address.</param>
        public static Guid DeviceIdFromAddress(string address)
        {
            var deviceGuid = new Byte[16];
            var macWithoutColons = address.Replace(":", "");
            var macBytes = Enumerable.Range(0, macWithoutColons.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(macWithoutColons.Substring(x, 2), 16))
                .ToArray();
            macBytes.CopyTo(deviceGuid, 10);

            return new Guid(deviceGuid);
        }

        #region IDevice implementation

        /// <summary>
        /// Occurs when services discovered.
        /// </summary>
        public event EventHandler<ServiceDiscoveredEventArgs> ServiceDiscovered = delegate { };

        /// <summary>
        /// Initiate a service discovery on the device
        /// </summary>
        public void DiscoverServices()
        {
			if (_gatt == null)
				Debug.WriteLine("{0}  Discover Services (gatt null)\t\tfor {1} ({2}) [{3}]", DateTime.Now.ToString("hh:mm:ss.fff tt"), _nativeDevice.Name, InstanceId,_nativeDevice.Address);
			else
				Debug.WriteLine("{0}  Discover Services (gatt ok)\t\tfor {1} ({2}) [{3}]", DateTime.Now.ToString("hh:mm:ss.fff tt"), _nativeDevice.Name, InstanceId, _nativeDevice.Address);

			Services = new ObservableCollection<IService>();
			Action action = () => _gatt.DiscoverServices();

            if (Configuration.DiscoverServicesOnMainThread)
            {
                var handler = new Android.OS.Handler(Android.OS.Looper.MainLooper);
                handler.PostAtFrontOfQueue(action);
            }
            else
            {
                action.Invoke();
            }
        }

        /// <summary>
        /// Discconnect from the device.
        /// </summary>
        public void Disconnect()
        {
            if (_gatt == null)
                return;

            try
            {
                //_gatt.Disconnect();
                //_gatt.Close();

                State = DeviceState.Disconnected;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private readonly Guid _id;
        /// <summary>
        /// Gets the unique identifier for the device
        /// </summary>
        /// <value>The device identifier</value>
        public Guid Id { get { return _id; } }

        /// <summary>
        /// Gets the device name
        /// </summary>
        /// <value>The device name</value>
        public string Name { get { return _nativeDevice.Name; } }

        private readonly int _rssi;
        /// <summary>
        /// Gets the Received Signal Strength Indicator
        /// </summary>
        /// <value>The RSSI in decibels</value>
        public int Rssi { get { return _rssi; } }

        /// <summary>
        /// Gets the native device object reference. Should be cast to the appropriate type.
        /// </summary>
        /// <value>The native device</value>
        public object NativeDevice { get { return _nativeDevice; } }

        /// <summary>
        /// Gets the state of the device
        /// </summary>
        /// <value>The device's state</value>
        public DeviceState State { get; set; }

        /// <summary>
		/// Gets scanRecord for the device.
		/// </summary>
		public byte[] ScanResponse { get; set;}

        /// <summary>
        /// Gets the discovered services for the device
        /// </summary>
        /// <value>The device's services</value>
        public ObservableCollection<IService> Services { get; set; }

		public string Address
		{
			get
			{
				return _nativeDevice.Address;
			}
		}


		#endregion

		#region GattCallback delegate methods

		private void ServicesDiscovered(object sender, EventArgs e)
        {
            foreach (var s in _gatt.Services)
            {
                var service = new Service(s, _gatt, _callback);
                Services.Add(service);

                ServiceDiscovered(this, new ServiceDiscoveredEventArgs(service));
            }
        }

		#endregion
		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			Debug.WriteLine("{0}  --- Disposing Device \t\t\t\t\t\t\tfor {1} ({2})", DateTime.Now.ToString("hh:mm:ss.fff tt"), Name,InstanceId);
			if (!disposedValue)
			{
				if (disposing)
				{
					if (Services.Count == 0)
						Debug.WriteLine("{0}       --- Services == 0");

					foreach (var service in Services)
					{
						service.Dispose();
					}
					Services = null;
				}
				if (_callback != null)
					_callback.ServicesDiscovered -= ServicesDiscovered;

				disposedValue = true;
			}
		}

		 ~Device() {
		   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		   Dispose(false);
		 }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			 GC.SuppressFinalize(this);
		}
		#endregion


	}
}