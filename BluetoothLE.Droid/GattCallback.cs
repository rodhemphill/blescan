﻿using System;
using Android.Bluetooth;
using BluetoothLE.Core;
using BluetoothLE.Core.Events;
using Android.Util;
using System.Diagnostics;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace BluetoothLE.Droid
{
	/// <summary>
	/// Gatt callback to handle Gatt events.
	/// </summary>
	public class GattCallback : BluetoothGattCallback
	{
		public string InstanceId;
		public ConcurrentQueue<Tuple<CommandQueueAction, BluetoothGattCharacteristic,byte[]>> CharacteristicCommandQueue = new ConcurrentQueue<Tuple<CommandQueueAction, BluetoothGattCharacteristic,byte[]>>();
		public string MyId;

		/// <summary>
		/// Occurs when device connected.
		/// </summary>
		public event EventHandler<DeviceConnectionEventArgs> DeviceConnected = delegate {};

		/// <summary>
		/// Occurs when device disconnected.
		/// </summary>
		public event EventHandler<DeviceConnectionEventArgs> DeviceDisconnected = delegate {};

		/// <summary>
		/// Occurs when services discovered.
		/// </summary>
		public event EventHandler ServicesDiscovered = delegate {};

		/// <summary>
		/// Occurs when characteristic value updated.
		/// </summary>
		public event EventHandler<CharacteristicReadEventArgs> CharacteristicValueUpdated = delegate {};

		/// <Docs>GATT client</Docs>
		/// <summary>
		/// Raises the connection state change event.
		/// </summary>
		/// <param name="gatt">Gatt.</param>
		/// <param name="status">Status.</param>
		/// <param name="newState">New state.</param>
		public override void OnConnectionStateChange(BluetoothGatt gatt, GattStatus status, ProfileState newState)
		{
			base.OnConnectionStateChange(gatt, status, newState);

			if (status != GattStatus.Success)
				return;
			
			Device device = null;
			//var device = new Device(gatt.Device, gatt, this, 0, null);

			try
			{
				//TODO the iterator may be being updated while we do this .. this may be better as a dictionary
				foreach (var dev in _adapterServices.BluetoothAdapter.DiscoveredDevices)
				{
					var nativeDev = (BluetoothDevice)dev.NativeDevice;
					if (gatt.Device.Address == nativeDev.Address)
					{
						device = (Device)dev;
						device.Callback = this;
						device.Gatt = gatt;
						Debug.WriteLine("{0}      native device address: {1}\t\t\t\t\tfor {2} ({3})", DateTime.Now.ToString("hh:mm:ss.fff tt"), 
						                nativeDev.Address, device.Name, device.InstanceId);
					}
				}
			} catch (Exception e) {
				Debug.WriteLine(e);
			}

			if (device == null)
				throw new Exception("GattCallback: couldn't find device");
			

             switch (newState)
			{
				case ProfileState.Disconnected:
                    device.State = DeviceState.Disconnected;

                    try
                    {
                        gatt.Close();
                        gatt = null;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Unable to close connection to gatt. Exception: {0}", ex.Message);
                    }
                    finally
                    {
                        DeviceDisconnected(this, new DeviceConnectionEventArgs(device));
                    }

					break;
				case ProfileState.Connected:
					device.State = DeviceState.Connected;

					AndroidBluetoothCommandQueue.Queue.Clear();

					_adapterServices.CurrentDevice = device;
                    DeviceConnected(this, new DeviceConnectionEventArgs(device));   
					break;
			}
		}

		/// <summary>
		/// Raises the services discovered event.
		/// </summary>
		/// <param name="gatt">Gatt.</param>
		/// <param name="status">Status.</param>
		public override void OnServicesDiscovered(BluetoothGatt gatt, GattStatus status)
		{
			Debug.WriteLine("{0}  Gatt Services Discovered\t\t\t\t\t\t\tin ({1})  Handle: {2}", DateTime.Now.ToString("hh:mm:ss.fff tt"),InstanceId,gatt.Handle);
			base.OnServicesDiscovered(gatt, status);

			if (status != GattStatus.Success)
				return;

			ServicesDiscovered(this, EventArgs.Empty);
		}


		/// <summary>
		/// Raises the characteristic read event.
		/// </summary>
		/// <param name="gatt">Gatt.</param>
		/// <param name="characteristic">Characteristic.</param>
		/// <param name="status">Status.</param>
		public override void OnCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			Debug.WriteLine("{0}  Native Read Response\t\t\t\t\t\t\t\t\tfor {1} ({2}) Gatt Handle: {3}", DateTime.Now.ToString("hh:mm:ss.fff tt"), 
			                characteristic.Uuid.ToString().Substring(0, 8),InstanceId,gatt.Handle);
			base.OnCharacteristicRead(gatt, characteristic, status);

			if (status != GattStatus.Success)
			{
				Debug.WriteLine("GattCallback: Characteristic Read failed on {0} because {1}", characteristic.Uuid.ToString().Substring(0, 8), status.ToString());
				AndroidBluetoothCommandQueue.Queue.Dequeue(gatt, characteristic);
				//this.UpdateReadQueue(characteristic);
				return;
			}

			var iChar = this.GetCharacteristic(characteristic, gatt);

			AndroidBluetoothCommandQueue.Queue.Dequeue(gatt, characteristic);
			CharacteristicValueUpdated(this, new CharacteristicReadEventArgs(iChar));


		}

	/// <Docs>GATT client the characteristic is associated with</Docs>
	/// <summary>
	/// Callback triggered as a result of a remote characteristic notification.
	/// </summary>
	/// <para tool="javadoc-to-mdoc">Callback triggered as a result of a remote characteristic notification.</para>
	/// <format type="text/html">[Android Documentation]</format>
	/// <since version="Added in API level 18"></since>
	/// <param name="gatt">Gatt.</param>
	/// <param name="characteristic">Characteristic.</param>
	public override void OnCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
		{
			Debug.WriteLine("{0}  Native Characteristic Changed\t\t\t\t\t\tfor {1} ({2}) Handle: {3}", DateTime.Now.ToString("hh:mm:ss.fff tt"), characteristic.Uuid.ToString().Substring(0, 8), InstanceId,gatt.Handle);
			base.OnCharacteristicChanged(gatt, characteristic);

			var iChar = this.GetCharacteristic(characteristic, gatt);
			//var iChar = new Characteristic(characteristic, gatt, this);
			CharacteristicValueUpdated(this, new CharacteristicReadEventArgs(iChar));
		}

		private ICharacteristic GetCharacteristic(BluetoothGattCharacteristic characteristic, BluetoothGatt gatt)
		{
			foreach (var service in _adapterServices.CurrentDevice.Services)
			{
				foreach (var icharacteristic in service.Characteristics)
				{
					if (icharacteristic.Uuid == characteristic.Uuid.ToString())
					{
						return icharacteristic;
					}
				}
			}
			var message = String.Format("Not a valid characteristic for this device (char: {0} device: {1})", characteristic.Uuid, _adapterServices.CurrentDevice.Name);
			throw new Exception(message);
		}

		public override void OnCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			base.OnCharacteristicWrite(gatt, characteristic, status);
			AndroidBluetoothCommandQueue.Queue.Dequeue(gatt, characteristic);
		}

		public override void OnDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, GattStatus status)
		{
			base.OnDescriptorRead(gatt, descriptor, status);
	
		}

		public override void OnDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, GattStatus status)
		{
			base.OnDescriptorWrite(gatt, descriptor, status);
			AndroidBluetoothCommandQueue.Queue.Dequeue(gatt, descriptor.Characteristic);
		}

	
		#region Constructor
		private IAdapterServices _adapterServices;
		public GattCallback()
		{
			_adapterServices = Settings.Container.Resolve<IAdapterServices>();

			InstanceId = String.Format("G{0}", DateTime.Now.ToString("mmss.fff"));
			Debug.WriteLine("{0}  New Gattcallback Created\t\t\t\t\t\t     ({1})", DateTime.Now.ToString("hh:mm:ss.fff tt"),  InstanceId);
		}
		#endregion
	}
}

