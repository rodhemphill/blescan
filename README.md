# README #

A simple Bluetooth Scanner created using Xamarin Forms and Prism.
It uses a generic iOS and Android implemented set of interfaces that implement the Bluetooth GATT protocol:
	IAdapter
	IDevice
	IService
	ICharacteristic

The GATT interfaces are created for iOS and Android.
The user interface project hasn't been created for Android - I will create this shortly.

### How do I get set up? ###

Modify bundle id in the info.plist and you're all good to go.

### Who do I talk to? ###

Rod Hemphill
MelbourneAppDevelopment.com
rod@melbappdev.com
