﻿using System.Collections.Generic;
using Microsoft.Practices.Unity;

namespace BluetoothLE
{
	public static class Settings
	{
		public static bool BluetoothLEDebugMode = true;

		public static int ScanTimeout = 10;  // seconds
		public static int ConnectionTimeout = 10;

		public static IUnityContainer Container;


	}
}
