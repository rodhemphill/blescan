﻿using System;
using System.Diagnostics;

namespace BluetoothLE.Core
{
	public class AdapterServices : IAdapterServices
	{
		public AdapterServices()
		{
		}
		private IAdapter _bluetoothAdapter = null;
		public IAdapter BluetoothAdapter
		{
			get
			{
				if (_bluetoothAdapter == null){
					Debug.WriteLine("\nWoops ... you forget to create the adapter!\n");

					return null;
					//throw new Exception("Woops ... you forget to create the adapter!");
				}
				return _bluetoothAdapter;
			}

			set
			{
				_bluetoothAdapter = value;
				_bluetoothAdapter.ScanTimeout = TimeSpan.FromSeconds(Settings.ScanTimeout);
				_bluetoothAdapter.ConnectionTimeout = TimeSpan.FromSeconds(Settings.ConnectionTimeout);

			}
		}
		public IDevice CurrentDevice { get; set;}
	}
}
