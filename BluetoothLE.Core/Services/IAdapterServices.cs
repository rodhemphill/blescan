﻿using System;
namespace BluetoothLE.Core
{
	public interface IAdapterServices
	{
		IAdapter BluetoothAdapter { get; set; }
		IDevice CurrentDevice { get; set;}
	}
}
