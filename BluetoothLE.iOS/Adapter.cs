﻿using System;
using CoreBluetooth;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using BluetoothLE.Core;
using BluetoothLE.Core.Events;
using System.Collections.ObjectModel;
using Foundation;
using System.Diagnostics;

namespace BluetoothLE.iOS
{
	/// <summary>
	/// Concrete implementation of <see cref="BluetoothLE.Core.IAdapter"/> interface.
	/// </summary>
	public class Adapter : IAdapter
	{
		private readonly CBCentralManager _central;
		private readonly AutoResetEvent _stateChanged;

		private static Adapter _current;

		/// <summary>
		/// Gets the current Adpater instance
		/// </summary>
		/// <value>The current Adapter instance</value>
		public static Adapter Current
		{
			get { return _current; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="BluetoothLE.iOS.Adapter"/> class.
		/// </summary>
		public Adapter()
		{
			_central = new CBCentralManager();

			_central.DiscoveredPeripheral += DiscoveredPeripheral;
			_central.UpdatedState += UpdatedState;
			_central.ConnectedPeripheral += ConnectedPeripheral;
			_central.DisconnectedPeripheral += DisconnectedPeripheral;
			_central.FailedToConnectPeripheral += FailedToConnectPeripheral;

			InstanceId = String.Format("A{0}", DateTime.Now.ToString("mmss.fff"));
			Debug.WriteLine("{0}  Create Adapter \t\t\t\t\t\t\t\t\t\t    ({1}) ", DateTime.Now.ToString("hh:mm:ss.fff tt"), InstanceId);

			ConnectedDevices = new List<IDevice>();
			DiscoveredDevices = new ObservableCollection<IDevice>();
			_stateChanged = new AutoResetEvent(false);

			_current = this;
		}

		private async Task WaitForState(CBCentralManagerState state)
		{
			while (_central.State != state)
			{
				await Task.Run(() => _stateChanged.WaitOne());
			}
		}

		#region IAdapter implementation

		/// <summary>
		/// Occurs when device discovered.
		/// </summary>
		public event EventHandler<DeviceDiscoveredEventArgs> DeviceDiscovered = delegate {};

		/// <summary>
		/// Occurs when device connected.
		/// </summary>
		public event EventHandler<DeviceConnectionEventArgs> DeviceConnected = delegate {};

		/// <summary>
		/// Occurs when device disconnected.
		/// </summary>
		public event EventHandler<DeviceConnectionEventArgs> DeviceDisconnected = delegate {};

		/// <summary>
		/// Occurs when scan timeout elapsed.
		/// </summary>
		public event EventHandler ScanTimeoutElapsed = delegate {};

		/// <summary>
		/// Occurs when a device failed to connect.
		/// </summary>
		public event EventHandler<DeviceConnectionEventArgs> DeviceFailedToConnect = delegate {};

		/// <summary>
		/// Gets or sets the amount of time to scan for devices.
		/// </summary>
		/// <value>The scan timeout.</value>
		public TimeSpan ScanTimeout { get; set; }

		/// <summary>
		/// Gets or sets the amount of time to attempt to connect to a device.
		/// </summary>
		/// <value>The connection timeout.</value>
		public TimeSpan ConnectionTimeout { get; set; }

		/// <summary>
		/// Start scanning for devices.
		/// </summary>
		public void StartScanningForDevices()
		{
			StartScanningForDevices(new string[0]);
		}

		/// <summary>
		/// Start scanning for devices.
		/// </summary>
		/// <param name="serviceUuids">White-listed service UUIDs</param>
		public async void StartScanningForDevices(params string[] serviceUuids)
		{
			await WaitForState(CBCentralManagerState.PoweredOn);

			var uuids = new List<CBUUID>();
			foreach (var guid in serviceUuids)
			{
				uuids.Add(CBUUID.FromString(guid));
			}

			DiscoveredDevices.Clear();
			IsScanning = true;

			_central.ScanForPeripherals(uuids.ToArray());

			await Task.Delay(ScanTimeout);

			if (IsScanning)
			{
				StopScanningForDevices();
				ScanTimeoutElapsed(this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// Stop scanning for devices.
		/// </summary>
		public void StopScanningForDevices()
		{
			IsScanning = false;
			_central.StopScan();
		}

		/// <summary>
		/// Connect to a device.
		/// </summary>
		/// <param name="device">The device.</param>
		public async void ConnectToDevice(IDevice device)
		{
			var peripheral = device.NativeDevice as CBPeripheral;
			_central.ConnectPeripheral(peripheral);

			await Task.Delay(ConnectionTimeout);

			if (ConnectedDevices.All(x => x.Id != device.Id))
			{
				_central.CancelPeripheralConnection(peripheral);
				var args = new DeviceConnectionEventArgs(device)
				{
					ErrorMessage = "The device connection timed out."
				};
				Debug.WriteLine("{0}  The device connection timed out\t\t\t\t\t\t\t\t\t\tfor {1} ", DateTime.Now.ToString("hh:mm:ss.fff tt"), device.Name  );

				DeviceFailedToConnect(this, args);
			}
		}

		/// <summary>
		/// Discconnect from a device.
		/// </summary>
		/// <param name="device">The device.</param>
		public void DisconnectDevice(IDevice device)
		{
			var peripheral = device.NativeDevice as CBPeripheral;
			_central.CancelPeripheralConnection(peripheral);
			Debug.WriteLine("{0}  CancelPeripheralConnection\t\t\t\t\t\t\t\t\t\tfor {1} ", DateTime.Now.ToString("hh:mm:ss.fff tt"), device.Name);
		}

		/// <summary>
		/// Gets a value indicating whether this instance is scanning.
		/// </summary>
		/// <value>true</value>
		/// <c>false</c>
		public bool IsScanning { get; set; }

		/// <summary>
		/// Gets the discovered devices.
		/// </summary>
		/// <value>The discovered devices.</value>
		//public IList<IDevice> DiscoveredDevices { get; set; }

		/// <summary>
		/// Gets the connected devices.
		/// </summary>
		/// <value>The connected devices.</value>
		public IList<IDevice> ConnectedDevices { get; set; }

		public ObservableCollection<IDevice> DiscoveredDevices { get; }

		//public IList<IDevice> Devices { get; set; }

		/// <summary>
		/// Clears the discovered devices.
		/// </summary>
		public void ClearDiscoveredDevices()
		{
			foreach (var device in DiscoveredDevices)
			{
				device.Dispose();
			}
		}
		public string InstanceId { get; set; }
		#endregion
		#region CBCentralManager delegate methods

		private async void DiscoveredPeripheral(object sender, CBDiscoveredPeripheralEventArgs e)
		{
			if (e.Peripheral.Name == null)
				return;


			var deviceId = Device.DeviceIdentifierToGuid(e.Peripheral.Identifier);
			foreach (var d in DiscoveredDevices)
			{
				if (d.Id == deviceId)
					return; // already exists
			}
			await Task.Delay(100); // give time for advertising data to appear

			if (Device.GetScanResponse(e.AdvertisementData) != null) // wait for advertising data
			{
				var device = new Device(e.Peripheral, e.AdvertisementData);
				DiscoveredDevices.Add(device);
				DeviceDiscovered(this, new DeviceDiscoveredEventArgs(device));
			}
		}

		private void UpdatedState(object sender, EventArgs e)
		{
			_stateChanged.Set();
		}

		private void ConnectedPeripheral(object sender, CBPeripheralEventArgs e)
		{
			var deviceId = Device.DeviceIdentifierToGuid(e.Peripheral.Identifier);
			Device device = null;
			foreach (var d in DiscoveredDevices)
			{
				if (d.Id == deviceId)
				{
					device = (Device)d;
					break;
				}
			}

			ConnectedDevices.Add(device);
			DeviceConnected(this, new DeviceConnectionEventArgs(device));
			//}
		}

		private void DisconnectedPeripheral(object sender, CBPeripheralErrorEventArgs e)
		{
			var deviceId = Device.DeviceIdentifierToGuid(e.Peripheral.Identifier);
			var connectedDevice = ConnectedDevices.FirstOrDefault(x => x.Id == deviceId);

			if (connectedDevice != null)
			{
				ConnectedDevices.Remove(connectedDevice);
				DeviceDisconnected(this, new DeviceConnectionEventArgs(connectedDevice));
			}
		}

		private void FailedToConnectPeripheral(object sender, CBPeripheralErrorEventArgs e)
		{
			var device = new Device(e.Peripheral);
			var args = new DeviceConnectionEventArgs(device) {
				ErrorMessage = e.Error.Description
			};

			DeviceFailedToConnect(this, args);
		}

		#endregion
	}
}

