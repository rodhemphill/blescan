﻿using System;

using Android.App;
using Android.Content.PM;
using Android.OS;
using Prism.Unity;
using Microsoft.Practices.Unity;
using BleScan.Droid;
using Android.Views;
using Xamarin.Forms;
using BleScan;
using BluetoothLE.Core;
using BluetoothLE.Droid;

namespace BleScanPump.Droid
{
	[Activity(Label = "BleScanPump.Droid", Icon = "@drawable/icon", Theme = "@style/BlackTheme", MainLauncher = true,ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = BleScan.Droid.Resource.Layout.Tabbar;
			ToolbarResource = BleScan.Droid.Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);

			App.ScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels);

			LoadApplication(new BleScan.App(new AndroidInitializer()));

		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			return false;

			//if (item.ItemId == 16908332) // This makes me feel dirty.
			//{
			//	var navPage = ((App.MainPage.Navigation.ModalStack[0] as MasterDetailPage).Detail as NavigationPage);

			//	if (app != null && navPage.Navigation.NavigationStack.Count > 0)
			//	{
			//		int index = navPage.Navigation.NavigationStack.Count - 1;

			//		var currentPage = navPage.Navigation.NavigationStack[index];

			//		var vm = currentPage.BindingContext as ViewModel;

			//		if (vm != null)
			//		{
			//			var answer = vm.OnBackButtonPressed();
			//			if (answer)
			//				return true;
			//		}

			//	}
			//}

			//return base.OnOptionsItemSelected(item);
		}
	}

	public class AndroidInitializer : IPlatformInitializer
	{
		public void RegisterTypes(IUnityContainer container)
		{
			DependencyService.Register<IAdapter, Adapter>();
		}
	}
}
