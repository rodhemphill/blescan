﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Prism.Unity;
using Microsoft.Practices.Unity;
using BluetoothLE.Core;
using BluetoothLE.iOS;
using Xamarin.Forms.Platform.iOS;
using XLabs.Forms.Controls;
using BleScan.iOS;

namespace BleScanPump.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
			{
				TextColor = UIColor.White
			});

			global::Xamarin.Forms.Forms.Init();

			// Set the status color
			UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
			UIApplication.SharedApplication.SetStatusBarHidden(false, false);

			DependencyService.Register<IAdapter, Adapter>();
			LoadApplication(new BleScan.App(new iOSInitializer()));

			// Set the tab bar style
			UITabBar.Appearance.SelectedImageTintColor = Color.FromHex("#fcbd36").ToUIColor();
			UITabBar.Appearance.BackgroundColor = UIColor.Black;
			UITabBar.Appearance.BarTintColor = UIColor.Black;
			UITabBar.Appearance.TintColor = UIColor.White;

			BleScan.App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;

			return base.FinishedLaunching(app, options);
		}
	}

	public class iOSInitializer : IPlatformInitializer
	{
		public void RegisterTypes(IUnityContainer container)
		{
		}
	}
}
