﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using BleScan;
using BleScan.CustomControls;
using BleScan.iOS;
using System;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace BleScan.iOS
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> args)
		{
			base.OnElementChanged(args);

			UITextField textField = (UITextField)Control;

			// Most commonly customized for font and no border
			//textField.Font = UIFont.FromName(DXStyle.FontFamily, (float)DXStyle.FontSmall.Size);
			textField.BorderStyle = UITextBorderStyle.None;
			textField.TintColor = UIColor.White;

			// Use 'Done' on keyboard
			textField.ReturnKeyType = UIReturnKeyType.Done;
			textField.EnablesReturnKeyAutomatically = true;

			// No auto-correct
			textField.AutocorrectionType = UITextAutocorrectionType.No;
			textField.SpellCheckingType = UITextSpellCheckingType.No;
			textField.AutocapitalizationType = UITextAutocapitalizationType.Words;

			// Misc.
			textField.ClearButtonMode = UITextFieldViewMode.WhileEditing;
			textField.ClearsOnBeginEditing = false;
			textField.ClearsOnInsertion = false;
			textField.AdjustsFontSizeToFitWidth = false;
			textField.KeyboardAppearance = UIKeyboardAppearance.Default;
		}
	}
}

