﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using BleScan;
using BleScan.CustomControls;
using BleScan.iOS;
using System;

[assembly: ExportRenderer(typeof(LongPressButton), typeof(LongPressButtonRenderer))]
namespace BleScan.iOS
{
	public class LongPressButtonRenderer : ButtonRenderer
	{

		// https://forums.xamarin.com/discussion/27323/how-can-i-recognize-long-press-gesture-in-xamarin-forms

		private LongPressButton longPressButton;
		public LongPressButtonRenderer()
		{
			this.AddGestureRecognizer(new UILongPressGestureRecognizer((longPress) =>
			{
				if (longPress.State == UIGestureRecognizerState.Began)
				{
					longPressButton.OnPressStarted();
				}
				else if (longPress.State == UIGestureRecognizerState.Ended)
				{
					longPressButton.OnPressEnded();
				}
			}));
		}

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement != null)
				longPressButton = e.NewElement as LongPressButton;
		}


		// https://forums.xamarin.com/discussion/26594/button-event-for-while-clicked

		//private UILongPressGestureRecognizer longPressGestureRecognizer;
		//private LongPressButton longPressButton;

		//protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		//{

		//	base.OnElementChanged(e);

		//	longPressGestureRecognizer = new UILongPressGestureRecognizer(() =>
		//	{

		//		// Notify element
		//		if (longPressGestureRecognizer.State == UIGestureRecognizerState.Began)
		//		{
		//			longPressButton.OnPressStarted();
		//		}
		//		else if (longPressGestureRecognizer.State == UIGestureRecognizerState.Ended)
		//		{
		//			longPressButton.OnPressEnded();
		//		}
		//	});
		//	longPressGestureRecognizer.MinimumPressDuration = 0.0;
		//	longPressGestureRecognizer.Delegate = gestureDelegate;

		//	AddGestureRecognizer(longPressGestureRecognizer);		
		//}
	}
}