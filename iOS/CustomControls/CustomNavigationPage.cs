﻿using BleScan.CustomControls;
using BleScan.iOS.CustomControls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomNavigationPage), typeof(CustomNavigationPageRenderer))]
    namespace BleScan.iOS.CustomControls
{
	public class CustomNavigationPageRenderer : NavigationRenderer
	{

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			this.NavigationBar.BackItem.Title = "Back";
			//this.NavigationBar.BarTintColor = UIColor.FromPatternImage(UIImage.FromFile("topbar_bg@2x.png"));
		}
	}
}
