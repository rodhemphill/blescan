﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using BleScan;
using BleScan.CustomControls;
using BleScan.iOS;
using System;

[assembly: ExportRenderer(typeof(ButtonWithPadding), typeof(ButtonWithPaddingRenderer))]
namespace BleScan.iOS
{
	public class ButtonWithPaddingRenderer : ButtonRenderer
	{
		//protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			var view = (ButtonWithPadding)this.Element;
			var nativeButton = this.Control;
			nativeButton.TitleEdgeInsets = new UIEdgeInsets((float)view.Padding.Top, (float)view.Padding.Left, (float)view.Padding.Bottom, (float)view.Padding.Right);
		}
	}
}

