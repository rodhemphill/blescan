﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using BleScan;
using BleScan.CustomControls;
using BleScan.iOS;
using System;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
namespace BleScan.iOS
{
	public class CustomTimePickerRenderer : TimePickerRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
		{
			base.OnElementChanged(e);

			var view = (TimePicker)Element;
			if (view != null)
			{
				var timePicker = (UIDatePicker)Control.InputView;
				timePicker.MinuteInterval = 15;

				Control.BorderStyle = UITextBorderStyle.None;
			}
		} 	}
}

